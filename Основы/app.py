from flask import Flask, render_template, request
import sys
import os

app = Flask(__name__)


@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html')


@app.route('/keyboard_handler', methods=['GET', 'POST'])
def handler():
    data_form = []
    if request.method == 'POST':
        try:
            if request.form['fio'] != '':
                data_form.append(request.form['fio'])
        except:
            data_form.append('НЕТ')

        try:
            if request.form['pass'] != '':
                data_form.append(request.form['pass'])
        except:
            data_form.append('НЕТ')
        
        
        data_form.append(request.form['disc'])
        
        try:
            if request.form['photoshop'] == 'Да':
                data_form.append(request.form['photoshop'])
        except:
            data_form.append('НЕТ')


        try:
            if request.form['dreamweaver'] == 'Да':
                data_form.append(request.form['dreamweaver'])
        except:
            data_form.append('НЕТ')

        try:
            if request.form['php'] == 'Да':
                data_form.append(request.form['php'])
        except:
            data_form.append('НЕТ')


        try:
            if request.form['dostavka'] != '':
                data_form.append(request.form['dostavka'])
        except:
            data_form.append('НЕТ')

        try:
            if request.form['adress'] != '':
                data_form.append(request.form['adress'])
        except:
            data_form.append('НЕТ')
            

    return render_template('handlers.html', data_form=data_form)


if __name__ == "__main__":
    app.run(debug=True)