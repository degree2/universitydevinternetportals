from flask import Flask, render_template, request
import os
import sqlite3


app = Flask(__name__)


@app.route('/')
@app.route('/index')
def index():
    return render_template('/index.html')


@app.route('/answers', methods=['GET', 'POST'])
def answer():
    answers = 0
    if request.method == 'POST':
        if request.form[f'q1'] == '1':
            answers += 1
        if request.form['q2'] == '1':
            answers += 1
        if request.form['q3'] == '1':
            answers += 1
        if request.form['q4'] == '1':
            answers += 1
        if request.form['q5'] == '1':
            answers += 1
        if request.form['q6'] == '1':
            answers += 1
        if request.form['q7'] == '1':
            answers += 1
        if request.form['q8'] == '1':
            answers += 1
        if request.form['q9'] == '1':
            answers += 1
        if request.form['q10'] == '1':
            answers += 1

    return f"Правильных ответов: {answers} <br/>"

if __name__ == "__main__":
    app.run(debug=True)